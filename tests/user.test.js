const dbHandler = require('./db-handler')
const User = require('../models/User')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const userComplete1 = {
  name: 'Aom',
  gender: 'F'
}

const userComplete2 = {
  name: 'Aom',
  gender: 'F'
}

const userErrorNameEmpty = {
  name: '',
  gender: 'F'
}

const userErrorName2aphabets = {
  name: 'Ao',
  gender: 'F'
}

const userErrorGenderInvalid = {
  name: 'Aom',
  gender: 'A'
}

describe('User', () => {
  it('สามารถเพิ่ม user ได้ M', async () => {
    let error = null
    try {
      const user = new User(userComplete1)
      await user.save()
      // await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('สามารถเพิ่ม user ได้ F', async () => {
    let error = null
    try {
      const user = new User(userComplete2)
      await user.save()
      // await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it(' ไม่สามารถเพิ่ม user ได้ เพราะ name เป็น ช่องว่าง', async () => {
    let error = null
    try {
      const user = new User(userErrorNameEmpty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it(' ไม่สามารถเพิ่ม user ได้ เพราะ name เป็น 2 ตัว', async () => {
    let error = null
    try {
      const user = new User(userErrorName2aphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it(' ไม่สามารถเพิ่ม user ได้ เพราะ gender ไม่ถูกต้อง', async () => {
    let error = null
    try {
      const user = new User(userErrorGenderInvalid)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it(' ไม่สามารถเพิ่ม user ได้ เพราะชื่อซ้ำกัน', async () => {
    let error = null
    try {
      const user1 = new User(userComplete1)
      await user1.save()
      const user2 = new User(userComplete1)
      await user2.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
